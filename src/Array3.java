import java.util.Scanner;

public class Array3 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Please input arr[" + i + "]: ");
            arr[i] = kb.nextInt();
        }
        System.out.print("arr = ");
        for (int j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + " ");
        }
    }
}
