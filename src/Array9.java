import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            System.out.println("Please input arr[" + i + "]: ");
            arr[i] = kb.nextInt();
        }
        System.out.print("arr = ");
        for (int j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + " ");
        }
        System.out.println();
        System.out.print("Please input search value : ");
        int searchnum = kb.nextInt();
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (searchnum == arr[i]) {
                index = i;
            }
        }
        if (index >= 0) {
            System.out.println("found at index: " + index);
        } else {
            System.out.println("not found");
        }

    }

}
