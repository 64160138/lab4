import java.lang.annotation.Target;
import java.util.Scanner;

public class Array12 {
    public static void main(String[] args) {
        while (true) {
            printWelcome();
            printMenu();
        }
    }

    private static void printWelcome() {
        System.out.println();
        System.out.print("Welcome to my app!!!!");
        System.out.println();
    }

    private static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1.Print Hello World N times");
        System.out.println("2.Add 2 number");
        System.out.println("3.Exit");
        inputChoice();
    }

    private static void inputChoice() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input your choice: ");
        int choice = sc.nextInt();
        if (choice == 1) {
            inputTime();
        } else if (choice == 2) {
            System.out.println("Please input first number : ");
            int first = sc.nextInt();
            System.out.println("Please input Second number : ");
            int second = sc.nextInt();
            System.out.print("Result = " + addTwoNumber(first, second));
        } else if (choice == 3) {
            exitProgram();
        } else {
            System.out.println("Error: Please input between 1-3 :");
            inputChoice();
        }
    }

    public static int inputTime() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please Enter a number of time : ");
        int times = sc.nextInt();
        printHelloWorldNtime(times);
        return times;
    }

    private static void printHelloWorldNtime(int times) {
        for (int i = 1; i <= times; i++) {
            System.out.println("Hello World!!!");
        }

    }

    private static int addTwoNumber(int first, int second) {
        return first + second;
    }

    private static void exitProgram() {
        System.out.println("Bye!!");
        System.exit(0);
    }
}
